class RPNCalculator
  attr_accessor :store
  def initialize
    @store = []
  end

  def value
    @value = @store[-1]
  end

  def push(el)
    @store << el * 1.0
  end

  def plus
    raise "calculator is empty" unless @store.length > 1
    a = @store.pop
    b = @store.pop
    @store << b + a
  end

  def minus
    raise "calculator is empty" unless @store.length > 1
    a = @store.pop
    b = @store.pop
    @store << b - a
  end

  def times
    raise "calculator is empty" unless @store.length > 1
    a = @store.pop
    b = @store.pop
    @store << a * b
  end

  def divide
    raise "calculator is empty" unless @store.length > 1
    a = @store.pop
    b = @store.pop
    @store << b / a
  end

  def tokens(string)
    string.split.map do |el|
      if el.to_i.to_s == el
        el.to_i
      else
        el.to_sym
      end
    end
  end

  def evaluate(string)
    tokens(string).each do |el|
      if el.is_a?(Integer)
        push(el)
      else
        a = @store.pop
        b = @store.pop
        @store << [b, a].reduce(el)
      end
    end
    value
  end
end
